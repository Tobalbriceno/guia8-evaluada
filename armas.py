#!/usr/bin/env python3
#-*- coding:utf-8 -*-


class Armas():
    def __init__(self):
        self._nombre = None
        self._danodado = None
        
    @property
    def nombre(self):
        return self._nombre
    
    @property
    def danodado(self):
        return self._danodado
